<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function getImageUrlAttribute()
    {
        return '/images/categories/'.$this->image;
    }
}
