<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;

    public function getImageUrlAttribute(){

        return '/images/pictures/'.$this->image;
    }

    public function getVideoUrlAttribute(){

        return '/videos/'.$this->video;
    }
}
