<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'apellido', 'email', 'password', 'rol_id', 'section_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getIsAdminAttribute() {
        return $this->role == 0;
    }

    public function getIsTeacherAttribute() {
        return $this->role == 1;
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }

    public function roles(){
        return $this->belongsTo(Rol::class);
    }

    public function sections(){
        return $this->hasOne(Section::class);
    }

}
