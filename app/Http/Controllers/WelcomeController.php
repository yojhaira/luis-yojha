<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{   
    public function index()
    {
        return view('welcomeNew');
    }

    // public function index()
    // {
    //     $categories = Category::all();
    //     return view('welcome', compact('categories'));
    // }

    public function editor()
    {
        return view('guest.editor');
    }

    public function home()
    {
        $categories = Category::all();
        return view('welcome', compact('categories'));
    }
}
