<?php

namespace App\Http\Controllers\Admin;

use App\Picture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class PictureImageController extends Controller
{
    public function index(Picture $picture)
    {
        return view('admin.pictures.images', compact('picture'));
    }

    public function store(Request $request, Picture $picture)
    {
        $picture->ornaments = $request->input('ornaments');
        $picture->save();

        $saved = $picture->save();
        return compact('saved');
    }

    public function upload(Request $request)
    {
        $file = $request->file('ornament');
        // dd($file);

        if ($file) {
            $extension = $file->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;
            $path = public_path('images/ornaments/' . $fileName);
            $success = File::move($file, $path);
            $url = asset('/images/ornaments/' . $fileName);
        } else {
            $success = false;
            $url = null;
        }

        return compact('success', 'url');
    }
}
