<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(10);

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:250',
            'image' => 'dimensions:min_width=150,min_height=120',
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre.',
            'name.max' => 'El nombre ingresado es demasiado extenso.',
            'image.dimensions' => 'La imagen debe tener como mínimo el tamaño de 150x120.'
        ];

        $this->validate($request, $rules, $messages);

        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        if ($request->hasFile('image')) {

            $imageInput = $request->file('image');
            $extension = $imageInput->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;

            $path = public_path('images/categories/' . $fileName);

            Image::make($imageInput)
                ->resize(150, 120)
                ->save($path);

            $category->image = $fileName;
        }
        $category->save();

        return redirect('/admin/categories');
    }

    public function edit($id, Request $request)
    {
        $category = Category::find($id);

        return view('admin.categories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required|string|max:250',
            'image' => 'dimensions:min_width=150,min_height=120',
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre.',
            'name.max' => 'El nombre ingresado es demasiado extenso.',
            'image.dimensions' => 'La imagen debe tener como mínimo el tamaño de 150x120.'
        ];

        $this->validate($request, $rules, $messages);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        if ($request->hasFile('image')) {
            if($category->image){
                $pathImage = public_path('/images/categories/' . $category->image);
                File::delete($pathImage);
            }

            $imageInput = $request->file('image');
            $extension = $imageInput->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;

            $path = public_path('images/categories/' . $fileName);

            Image::make($imageInput)
                ->fit(150, 120, function ($constraint) {
                    $constraint->upsize();
                })->save($path);

            $category->image = $fileName;
        }
        $category->save();

        return redirect('/admin/categories');
    }

    public function delete($id)
    {
        $category = Category::find($id);
        $pathImage = public_path('/images/categories/' . $category->image);
        File::delete($pathImage);
        $category->delete();

        return back();
    }
}
