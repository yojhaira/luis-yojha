<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Picture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class PictureController extends Controller
{

    public function index(Request $request)
    {
        $query = Picture::query();
        $categories = Category::all();
        $categoryId = $request->category_id;
        if($categoryId)
            $query = $query->where('category_id', $categoryId);

        $pictures = $query->paginate(10);

        return view('admin.pictures.index', compact('categories', 'pictures', 'categoryId'));
    }

    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('admin.pictures.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:250',
            'image' => 'required',
            'video' => 'required| mimes:mp4',
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre.',
            'name.max' => 'El nombre ingresado es demasiado extenso.',
            'image.required' => 'Es necesario ingresar una imagen.',
            'video.required' => 'Es necesario ingresar un video guía.',
            'video.mimes' => 'El video debe ser formato MP4.',
        ];

        $request->validate($rules, $messages);

        $picture = new Picture();
        $picture->name = $request->name;
        $picture->description = $request->description;

        if ($request->hasFile('image')) {
            $imageInput = $request->file('image');
            $extension = $imageInput->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;
            $path = public_path('images/pictures/' . $fileName);
            File::move($imageInput,$path);
            $picture->image = $fileName;
        }
        if ($request->hasFile('video')) {
            $videoInput = $request->file('video');
            $extension = $videoInput->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;
            $path = public_path('videos/' . $fileName);
            File::move($videoInput,$path);
            $picture->video = $fileName;
        }

        $picture->category_id = $request->category_id;
        $picture->save();

        return redirect('/admin/pictures');
    }

    public function edit($id)
    {
        $picture = Picture::find($id);
        $categories = Category::orderBy('name')->get();

        return view('admin.pictures.edit', compact('picture', 'categories'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required|string|max:250',
            'category_id' => 'required|exists:categories,id'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre.',
            'name.max' => 'El nombre ingresado es demasiado extenso.'
        ];

        $request->validate($rules, $messages);

        $picture = Picture::find($id);
        $picture->name = $request->name;
        $picture->category_id = $request->category_id;
        $picture->description = $request->description;

        if ($request->hasFile('image')) {
            if($picture->image){
                $pathImage = public_path('/images/pictures/' . $picture->image);
                File::delete($pathImage);
            }

            $imageInput = $request->file('image');
            $extension = $imageInput->getClientOriginalExtension();
            $fileName = uniqid() . '.' . $extension;
            $path = public_path('images/pictures/' . $fileName);
            File::move($imageInput,$path);
            $picture->image = $fileName;
        }

        if ($request->hasFile('video')) {
            if($picture->video){
                $pathVideo = public_path('/videos/' . $picture->video);
                File::delete($pathVideo);
            }

            $videoInput = $request->file('video');
            $extension = $videoInput->getClientOriginalExtension();
            $videoName = uniqid() . '.' . $extension;
            $path = public_path('videos/' . $videoName);
            File::move($videoInput,$path);
            $picture->video = $videoName;
        }
        $picture->save();

        return redirect('/admin/pictures');
    }

    public function getGuide(Picture $picture)
    {
        return view('admin.pictures.guide', compact('picture'));
    }

    public function postGuide(Picture $picture, Request $request)
    {
        $picture->guide = $request->guide;
        $saved = $picture->save();

        return compact('saved');
    }

    public function delete($id)
    {
        $picture = Picture::find($id);
        $pathImage = public_path('/images/pictures/' . $picture->image);
        $pathVideo = public_path('/videos/' . $picture->video);
        File::delete($pathImage);
        File::delete($pathVideo);
        $picture->delete();

        return back();
    }
}
