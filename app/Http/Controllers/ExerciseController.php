<?php

namespace App\Http\Controllers;

use App\Category;
use App\Match;
use App\Picture;
use Illuminate\Http\Request;

class ExerciseController extends Controller
{
    public function index($category_id, $picture_id)
    {
        $category = Category::find($category_id);
        $picture = Picture::find($picture_id);

        return view('exercise', compact('category', 'picture'));
    }

    public function play(Picture $picture)
    {
        return view('student.play', compact('picture'));
    }

    public function store(Request $request, $pictureId)
    {
        $match = new Match();

        $match->user_id = auth()->id();
        $match->picture_id = $pictureId;

        $match->precision = $request->precision;
        $match->coverage = $request->coverage;

        $match->successful = $request->successful;
        $saved = $match->save();

        return compact('saved');
    }

    public function next(Request $request)
    {
        $pictureId = $request->input('picture_id');
        $picture = Picture::find($pictureId);

        if (!$picture)
            return redirect('/');

        $categoryId = $picture->category_id;
        $nextPicture = Picture::where('id', '>', $pictureId)->where('category_id', $categoryId)
            ->orderBy('id', 'asc')->value('id');

        if ($nextPicture) {
            return redirect("/category/$categoryId/pictures/$nextPicture");
        } else {
            return redirect("/category/$categoryId/pictures");
        }
    }
}
