<?php

namespace App\Http\Controllers\Teacher;

use App\Category;
use App\Picture;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function index()
    {
        $students = User::where('teacher_id', auth()->id())->paginate(10);

        return view('teacher.students.index', compact('students'));
    }

    public function reports($id)
    {
        $student = User::find($id);

        // 1
        $matches = $student->matches()->get(['successful']);
        if (count($matches) > 0) {
            $successful = $matches->where('successful', true)->count();
            $hits = intval($successful * 100 / count($matches));
            $mistakes = 100 - $hits;
        } else {
            $hits = 0;
            $mistakes = 0;
        }

        $gaugeReport = compact('hits', 'mistakes');

        // 2
        $categories = Category::get(['id', 'name']);
        $exerciseCounts = [];
        $exerciseTotals = [];

        foreach ($categories as $category) {
            $pictureIds = Picture::where('category_id', $category->id)->pluck('id');
            $exerciseTotals[] = count($pictureIds);

            $count = $student->matches()->whereIn('picture_id', $pictureIds)
                ->distinct('picture_id')->where('successful', true)->count();
            $exerciseCounts[] = $count;
        }

        $levelsReport = [
            'names' => $categories->pluck('name'),
            'counts' => $exerciseCounts,
            'totals' => $exerciseTotals
        ];

        // 3
        $pictureIds = $student->matches()->distinct('picture_id')->pluck('picture_id');
        $pictureNames = Picture::whereIn('id', $pictureIds)->pluck('name');
        $exerciseTimes = [];

        foreach ($pictureIds as $pictureId) {
            $times = $student->matches()->where('picture_id', $pictureId)->count();
            $exerciseTimes[] = $times;
        }

        $attemptsReport = [
            'exercises' => $pictureNames,
            'times' => $exerciseTimes
        ];

        return view('teacher.students.reports', compact('student', 'gaugeReport', 'levelsReport', 'attemptsReport'));
    }

    public function create()
    {
        return view('teacher.students.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del médico',
            'email.required' => 'Es necesario ingresar el e-mail del médico.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.required' => 'Es necesario ingresar una contraseña',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $student = new User();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->role = 2;
        $student->password = bcrypt($request->password);
        $student->teacher_id = auth()->id();
        $student->save();

        return redirect('/teacher/students');
    }

    public function edit($id)
    {
        $student = User::find($id);

        return view('teacher.students.edit', compact('student'));
    }

    public function update($id, Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => "required|email|max:255|unique:users,email,$id",
            'password' => 'nullable|min:6'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar el nombre del médico',
            'email.required' => 'Es necesario ingresar el e-mail del médico.',
            'email.email' => 'El e-mail ingresado no es válido.',
            'email.max' => 'El e-mail es demasiado extenso.',
            'email.unique' => 'El e-mail ya se encuentra en uso.',
            'password.min' => 'La contraseña debe presentar al menos 6 caracteres.',
        ];

        $this->validate($request, $rules, $messages);

        $student = User::find($id);
        $student->name = $request->name;
        $student->email = $request->email;
        $password = $request->password;
        if($password)
            $student->password = bcrypt($password);
        $student->save();

        return redirect('/teacher/students');
    }

    public function delete($id)
    {
        $student = User::find($id);
        $student->delete();

        return back();
    }
}
