<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolPermiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('rol_permiso', function ( Blueprint $table){

            // FK 1
            $table->integer('rol_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('rol');

            // FK 2
            $table->integer('permiso_id')->unsigned();
            $table->foreign('permiso_id')->references('id')->on('permiso');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_permiso');
    }
}
