<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     User::create([
             'name' => 'Yojhaira',
             'apellido' => 'Gonzales',
             'email' => 'yojhaira.gonzales@gmail.com',
             'password' => bcrypt('123123'),
             'rol_id' => 1,
             'section_id' =>1,
       ]);

        // User::create([
        //     'name' => 'Profesor',
        //     'email' => 'profesor@gmail.com',
        //     'role' => 1,
        //     'password' => bcrypt('123123')
        // ]);

        // User::create([
        //     'name' => 'Alumno',
        //     'email' => 'alumno@gmail.com',
        //     'role' => 2,
        //     'password' => bcrypt('123123'),
        //     'teacher_id' => 2
        // ]);
     }
}
