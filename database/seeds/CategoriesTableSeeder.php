<?php

use App\Category;
use App\Picture;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   DB::statement('SET FOREIGN_KEY_CHECKS = 0;');//desactiva la revision de claves foráneas
        DB::table('categories')->truncate();//elimina registros
        DB::table('pictures')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');//activa la revision de claves foráneas

        Category::create(['name' => 'Figuras', 'image' => '1.png']);
        Category::create(['name' => 'Palabras', 'image' => '2.png']);
        Category::create(['name' => 'Ejercicios', 'image' => '3.png']);
        Category::create(['name' => 'Dibujo Libre', 'image' => '4.png']);
        Category::create(['name' => 'Reportes', 'image' => '5.png']);

        Picture::create(['name' => 'Letra A', 'category_id' => 1, 'image' => 'letraA.png', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Letra B', 'category_id' => 1, 'image' => 'letraB.png', 'video' => 'letra_B.mp4']);
        Picture::create(['name' => 'Letra C', 'category_id' => 1, 'image' => 'letraC.png', 'video' => 'letra_C.mp4']);
        Picture::create(['name' => 'Letra D', 'category_id' => 1, 'image' => 'letraD.png', 'video' => 'letra_D.mp4']);  
        Picture::create(['name' => 'Letra E', 'category_id' => 1, 'image' => 'letraE.png', 'video' => 'letra_E.mp4']);
        Picture::create(['name' => 'Letra F', 'category_id' => 1, 'image' => 'letraF.png', 'video' => 'letra_F.mp4']);
        Picture::create(['name' => 'Letra G', 'category_id' => 1, 'image' => 'letraG.png', 'video' => 'letra_G.mp4']);
        Picture::create(['name' => 'Letra H', 'category_id' => 1, 'image' => 'letraH.png', 'video' => 'letra_H.mp4']);
        Picture::create(['name' => 'Letra I', 'category_id' => 1, 'image' => 'letraI.png', 'video' => 'letra_I.mp4']);
        Picture::create(['name' => 'Letra J', 'category_id' => 1, 'image' => 'letraJ.png', 'video' => 'letra_J.mp4']);
        Picture::create(['name' => 'Letra K', 'category_id' => 1, 'image' => 'letraK.png', 'video' => 'letra_K.mp4']);
        Picture::create(['name' => 'Letra L', 'category_id' => 1, 'image' => 'letraL.png', 'video' => 'letra_L.mp4']);
        Picture::create(['name' => 'Letra M', 'category_id' => 1, 'image' => 'letraM.png', 'video' => 'letra_M.mp4']);
        Picture::create(['name' => 'Letra N', 'category_id' => 1, 'image' => 'letraN.png', 'video' => 'letra_N.mp4']);
        Picture::create(['name' => 'Letra O', 'category_id' => 1, 'image' => 'letraO.png', 'video' => 'letra_O.mp4']);
        Picture::create(['name' => 'Letra P', 'category_id' => 1, 'image' => 'letraP.png', 'video' => 'letra_P.mp4']);
        Picture::create(['name' => 'Letra Q', 'category_id' => 1, 'image' => 'letraQ.png', 'video' => 'letra_Q.mp4']);
        Picture::create(['name' => 'Letra R', 'category_id' => 1, 'image' => 'letraR.png', 'video' => 'letra_R.mp4']);
        Picture::create(['name' => 'Letra S', 'category_id' => 1, 'image' => 'letraS.png', 'video' => 'letra_S.mp4']);
        Picture::create(['name' => 'Letra T', 'category_id' => 1, 'image' => 'letraT.png', 'video' => 'letra_T.mp4']);
        Picture::create(['name' => 'Letra U', 'category_id' => 1, 'image' => 'letraU.png', 'video' => 'letra_U.mp4']);
        Picture::create(['name' => 'Letra V', 'category_id' => 1, 'image' => 'letraV.png', 'video' => 'letra_V.mp4']);
        Picture::create(['name' => 'Letra W', 'category_id' => 1, 'image' => 'letraW.png', 'video' => 'letra_W.mp4']);
        Picture::create(['name' => 'Letra X', 'category_id' => 1, 'image' => 'letraX.png', 'video' => 'letra_X.mp4']);
        Picture::create(['name' => 'Letra Y', 'category_id' => 1, 'image' => 'letraY.png', 'video' => 'letra_Y.mp4']);
        Picture::create(['name' => 'Letra Z', 'category_id' => 1, 'image' => 'letraZ.png', 'video' => 'letra_Z.mp4']);


        Picture::create(['name' => 'Reconoce las vocales', 'category_id' => 2, 'image' => '1.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Trazar las vocales', 'category_id' => 2, 'image' => '2.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Identifica letras', 'category_id' => 2, 'image' => '3.jpg', 'video' => 'letra_A.mp4']);

        Picture::create(['name' => 'Reconoce las vocales', 'category_id' => 3, 'image' => '1.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Trazar las vocales', 'category_id' => 3, 'image' => '2.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Identifica letras', 'category_id' => 3, 'image' => '3.jpg', 'video' => 'letra_A.mp4']);

        Picture::create(['name' => 'Reconoce las vocales', 'category_id' => 4, 'image' => '1.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Trazar las vocales', 'category_id' => 4, 'image' => '2.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Identifica letras', 'category_id' => 4, 'image' => '3.jpg', 'video' => 'letra_A.mp4']);

        Picture::create(['name' => 'Reconoce las vocales', 'category_id' => 5, 'image' => '1.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Trazar las vocales', 'category_id' => 5, 'image' => '2.jpg', 'video' => 'letra_A.mp4']);
        Picture::create(['name' => 'Identifica letras', 'category_id' => 5, 'image' => '3.jpg', 'video' => 'letra_A.mp4']);

    }
}
