let myCanvas;
const PRECISION_TOLERANCE = 12;
const COVERAGE_TOLERANCE = 10;

const Alert = {
    emptyDrawing: function () {
        document.getElementById('audioEmpty').play();

        swal('Hey!', 'Todavía no hiciste ningún trazo.', 'danger', {
            button: 'Empezar ahora',
            icon: '/images/alerts/failure.png'
        });
    },

    successfulDrawing: function () {
        document.getElementById('audioSuccess').play();

        swal('Genial!', 'Has completado el trazo correctamente.', 'success', {
            buttons: {
                cancel: 'Ir a la lista de ejercicios',
                catch: {
                    text: 'Siguiente ejercicio!',
                    value: 'next'
                }
            },
            icon: '/images/alerts/success.png'
        }).then(value => {
            switch (value) {
                case 'next':
                    document.getElementById('btnGoNext').click();
                    break;

                default:
                    document.getElementById('btnGoCategory').click();
            }
        });
    },

    badPrecision: function () {
        document.getElementById('audioTryAgain').play();

        swal('Oops!', 'Intenta no alejarte mucho de la guía. Dibuja al interior de ella.', 'danger', {
            buttons: {
                cancel: 'Continuar dibujando',
                catch: {
                    text: 'Volver a empezar!',
                    value: 'clear'
                }
            },
            icon: '/images/alerts/bad-precision.png'
        }).then(value => {
            switch (value) {
                case 'clear':
                    newCanvas();
                    break;
            }
        });
    },

    incompleteDrawing: function () {
        document.getElementById('audioIncomplete').play();

        swal('Aún te falta completar el trazo!', 'Sigue la línea guía de color gris.', 'danger', {
            buttons: {
                cancel: 'Continuar dibujando',
                catch: {
                    text: 'Volver a empezar!',
                    value: 'clear'
                }
            },
            icon: '/images/alerts/incomplete.gif'
        }).then(value => {
            switch (value) {
                case 'clear':
                    newCanvas();
                    break;
            }
        });
    }
};

// wait for device init and setup a new canvas
document.addEventListener('DOMContentLoaded', newCanvas, false);

function newCanvas() {
    // define canvas size
    const canvasWidth = window.innerWidth;
    const canvasHeight = window.innerHeight - 44;

    // init canvas element
    const content = document.getElementById('content');
    content.style.height = canvasHeight;
    content.innerHTML = `<canvas id="canvas" width="${canvasWidth}" height="${canvasHeight}"></canvas>`;

    // setup canvas
    const canvas = document.getElementById('canvas');
    myCanvas = new DrawingArea(canvas, true);
    myCanvas.setOffset(0, 44);

    if (guideHistory) {
        drawGuide();
        drawOrnaments();
    } else {
        alert('Aún no se ha definido una guía para este ejercicio.');
        location.href = '/';
    }
}

// 2 evaluations: precision and coverage
function compareDrawings() {
    const guidePoints = actionsToPoints(guideHistory);
    const drawnPoints = actionsToPoints(myCanvas.history);

    if (drawnPoints.length === 0) {
        Alert.emptyDrawing();
        return;
    }

    // precision: right vs wrong (useless) lines
    let rightPoints = 0;
    drawnPoints.forEach(drawnPoint => {
        const isRightPoint = guidePoints.some(point => distanceBetween(drawnPoint, point) < PRECISION_TOLERANCE);

        if (isRightPoint)
            rightPoints += 1;
    });
    const precision = rightPoints / drawnPoints.length * 100;
    console.log('Right and total drawn points', rightPoints, drawnPoints.length);
    console.log('Precision: ' + precision);

    // coverage: how much the guide was fulfilled
    let fulfilledPoints = 0;
    guidePoints.forEach(guidePoint => {
        const isFulfilledPoint = drawnPoints.some(point => distanceBetween(guidePoint, point) < COVERAGE_TOLERANCE);

        if (isFulfilledPoint)
            fulfilledPoints += 1;
    });
    const coverage = fulfilledPoints / guidePoints.length * 100;
    console.log('Fulfilled and total guide points', fulfilledPoints, guidePoints.length);
    console.log('Coverage: ' + coverage);

    let successful;
    if (precision > 96 && coverage > 97) {
        Alert.successfulDrawing();
        successful = true;
    } else if (precision <= 96) {
        Alert.badPrecision();
        successful = false;
    } else if (coverage <= 97) {
        Alert.incompleteDrawing();
        successful = false;
    }

    const dataset = document.getElementById('page').dataset;
    const url = dataset.action;
    const data = JSON.stringify({
        precision, coverage, successful
    });

    const fetchData = {
        method: 'POST',
        credentials: 'same-origin',
        body: data,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": dataset.csrf
        }
    };

    fetch(url, fetchData)
        .then(r => r.json())
        .then(response => {
            console.log(response);
            /*
            if (response.saved && successful)
                document.getElementById('btnBack').click();
            */
        });

}

function drawGuide() {
    myCanvas.drawGuide(guideHistory);
}

function actionsToPoints(actions) {
    return actions
        .filter(action => action.type === 'draw')
        .map(action => {
            const {type, ...point} = action;
            return point;
        });
}

function distanceBetween(p1, p2) {
    const x = p1.x - p2.x;
    const y = p1.y - p2.y;

    return Math.sqrt(x*x + y*y);
}

function drawOrnaments() {
    ornaments.forEach(ornament => {
        const img = new Image();

        img.onload = () => {
            ornament.image = img;
            myCanvas.drawImage(ornament.image, ornament.x, ornament.y);
        };

        img.src = ornament.url;
    });
}