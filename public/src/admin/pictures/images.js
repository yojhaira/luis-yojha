let myCanvas;
let isFirstTime = true;
let $inputImage;

document.addEventListener('DOMContentLoaded', newCanvas, false);

function newCanvas() {
	// define and resize canvas
	const content = document.getElementById('content');
    content.style.height = (window.innerHeight - 90).toString();
    content.innerHTML = '<canvas id="canvas" width="' + window.innerWidth + '" height="' + (window.innerHeight - 90) + '"></canvas>';
    
    // setup canvas
	const canvas = document.getElementById('canvas');
    const saveMode = false; // to not record
    const readOnly = true; // to not listen for drawing events

	myCanvas = new DrawingArea(canvas, saveMode, readOnly);
	myCanvas.setOffset(0, 44);

	if (isFirstTime) {
	    $inputImage = document.getElementById('inputImage');
	    isFirstTime = false;
        loadExistingOrnaments();
    }

    drawGuideHistory();
    setupLoadImageEvent();
}

function saveCanvas() {
    const ornamentsAsString = JSON.stringify(ornaments);
    const data = JSON.stringify({
        ornaments: ornamentsAsString
    });
    // console.log(data);

    const fetchData = {
        method: 'POST',
        credentials: 'same-origin',
        body: data,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": csrfToken
        }
    };

    fetch(storeUrl, fetchData)
        .then(r => r.json())
        .then(response => {
            console.log(response);
            if (response.saved)
                location.href = '/admin/pictures';
        });
}

function drawGuideHistory() {
    if (myHistory)
        myCanvas.drawHistory(myHistory);
}

function loadOrnament() {
    $inputImage.click();
}

function setupLoadImageEvent() {
    $inputImage.onchange = e => {
        const reader = new FileReader();

        reader.onload = event => {
            const imgObj = new Image();
            imgObj.src = event.target.result;

            imgObj.onload = () => {
                const ornament = {
                    width: imgObj.width,
                    height: imgObj.height,
                    x: 0,
                    y: 0,
                    image: imgObj
                };

                const formData = new FormData();
                formData.append('ornament', $inputImage.files[0]);
                // data.append('user', 'jc');

                if (ornament.width < 600 && ornament.height < 600) {
                    fetch('/admin/pictures/ornament', {
                        method: 'POST',
                        body: formData,
                        credentials: 'same-origin',
                        headers: {
                            "X-CSRF-Token": csrfToken
                        }
                    })
                    .then(response => response.json())
                    .then(data => {
                        if (data.success) {
                            ornament.url = data.url;
                            ornaments.push(ornament);
                            registerOrnament(ornament);
                        }
                    })
                    .catch(
                        error => console.log(error) // Handle the error response object
                    );
                } else {
                    alert('Ni el ancho ni el alto deben superar los 600px');
                }

            };
        };

        reader.readAsDataURL(e.target.files[0]);
    }
}

const registerOrnament = function (ornament) {
    const offsetX = 0;
    const offsetY = 44;

    let startX, startY, isDragging;

    function hitImage() {
        return (startX > ornament.x && startX < ornament.x + ornament.width
            && startY > ornament.y && startY < ornament.y + ornament.height);
    }

    function handleMouseDown(e) {
        startX = Math.round(e.clientX - offsetX);
        startY = Math.round(e.clientY - offsetY);
        // console.log('mouse down', startX, startY);
        // set the drag flag
        isDragging = hitImage();
    }

    function handleMouseUp(e) {
        startX = Math.round(e.clientX - offsetX);
        startY = Math.round(e.clientY - offsetY);
        // clear the drag flag
        isDragging = false;
    }

    function handleMouseMove(e) {
        // console.log('mouse move');
        // only compute new positions if in drag
        if (isDragging) {

            const canMouseX = Math.round(e.clientX - offsetX);
            const canMouseY = Math.round(e.clientY - offsetY);

            // move the image by the amount of the latest drag
            const dx = canMouseX - startX;
            const dy = canMouseY - startY;

            ornament.x += dx;
            ornament.y += dy;

            // Avoid negative image locations
            ornament.y = Math.max(0, ornament.y);
            ornament.x = Math.max(0, ornament.x);

            // reset the startXY for next time
            startX = canMouseX;
            startY = canMouseY;

            myCanvas.clear();
            drawGuideHistory();
            drawOrnaments();
        }
    }

    myCanvas.drawImage(ornament.image, ornament.x, ornament.y);

    const canvas = document.getElementById('canvas');
    canvas.addEventListener('mousedown', handleMouseDown);
    canvas.addEventListener('mousemove', handleMouseMove);
    canvas.addEventListener('mouseup', handleMouseUp);
    canvas.addEventListener('mouseout', handleMouseUp);
};

function drawOrnaments() {
    ornaments.forEach(ornament =>
        myCanvas.drawImage(ornament.image, ornament.x, ornament.y)
    );
}

function loadExistingOrnaments() {
    ornaments.forEach(ornament => {
        const img = new Image();

        img.onload = () => {
            ornament.image = img;
            registerOrnament(ornament);
        };

        img.src = ornament.url;
    });
}