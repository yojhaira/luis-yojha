let myCanvas;
let isFirstTime = true;
let $inputImage;

// wait for device init and setup a new canvas 
document.addEventListener('DOMContentLoaded', newCanvas, false);

function newCanvas() {
	// define and resize canvas
	const content = document.getElementById('content');
    content.style.height = (window.innerHeight - 90).toString();
    content.innerHTML = '<canvas id="canvas" width="' + window.innerWidth + '" height="' + (window.innerHeight - 90) + '"></canvas>';
    
    // setup canvas
	const canvas = document.getElementById('canvas');
    const saveMode = true; // to keep a history of all actions

	myCanvas = new DrawingArea(canvas, saveMode);
	myCanvas.setOffset(0, 44);

	if (isFirstTime) {
	    $inputImage = document.getElementById('inputImage');
	    loadCanvas();
	    isFirstTime = false;
    }

    setupLoadImageEvent();
}

function saveCanvas() {
    myHistory = myCanvas.history.filter(action => action.type !== 'color');

    const dataset = document.getElementById('page').dataset;
    const url = dataset.action;
    const guideAsString = JSON.stringify(myHistory);
    const data = JSON.stringify({
        guide: guideAsString
    });
    // console.log(data);

    const fetchData = {
        method: 'POST',
        credentials: 'same-origin',
        body: data,
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-Token": dataset.csrf
        }
    };

    fetch(url, fetchData)
        .then(r => r.json())
        .then(response => {
            console.log(response);
            if (response.saved)
                location.href = '/admin/pictures';
        });
}

function loadCanvas() {
    if (myHistory)
        myCanvas.drawHistory(myHistory, true);
}

function loadImage() {
    $inputImage.click();
}

function setupLoadImageEvent() {
    $inputImage.onchange = e => {
        const reader = new FileReader();

        reader.onload = event => {
            const imgObj = new Image();
            imgObj.src = event.target.result;

            imgObj.onload = () => {
                myCanvas.drawImage(imgObj);

                // redraw (to avoid image to be drawn upper)
                if (myHistory)
                    myCanvas.drawHistory(myHistory);
            };
        };

        reader.readAsDataURL(e.target.files[0]);
    }
}