function renderLevels(categories) {
    Highcharts.chart('levels', {
        credits: {
            enabled: false
        },
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Ejercicios resueltos por categoría'
        },
        xAxis: {
            categories: categories['names']
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: '%'
            }
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            shared: true
        },
        legend: {
            reversed: true
        },
        series: [{
            name: 'Ejercicios Resueltos',
            data: categories['counts']
        }, {
            name: 'Ejercicios Propuestos',
            data: categories['totals']
        }]
    });
}
