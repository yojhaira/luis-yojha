function renderAttempts(attempts) {
    Highcharts.chart('attempts', {
        credits: {
            enabled: false
        },
        chart: {
            type: 'column'
        },
        title: {
            text: '# Veces que se ha evaluado cada ejercicio'
        },
        xAxis: {
            categories: attempts['exercises']
        },
        yAxis: {
            min: 0,
            title: false
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Intentos',
            data: attempts['times']
        }]
    });
}