<!doctype html>
<html>
<head>
    <title>Resolver dibujo {{ $picture->id }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{ asset('vendor/sketch/sketch.css') }}">
</head>
<body>

<div id="page" data-action="{{ url('/pictures/'.$picture->id.'/play') }}" data-csrf="{{ csrf_token() }}">
    <div class="header">
        <div class="title">Resolviendo "{{ $picture->name }}"</div>
    </div>

    <div id="content">
        <p style="text-align:center">Cargando Canvas ...</p>
    </div>

    <div class="footer">
        <div class="buttons">
            <a id="btnGoCategory" style="display: none" href="{{ url('/category/'.$picture->category_id.'/pictures') }}"></a>
            <a id="btnGoNext" style="display: none" href="{{ url('/next?picture_id='.$picture->id) }}"></a>

            <a id="btnBack" class="navbtn" href="{{ url('/category/'.$picture->category_id.'/pictures/'.$picture->id) }}">< Volver</a>
            <a class="navbtn" onclick="newCanvas()">Limpiar</a>
            <a class="navbtn" onclick="compareDrawings()">Evaluar</a>
        </div>
    </div>
</div>

<audio id="audioSuccess" src="{{ asset('/sounds/lograste_aplausos.mp3') }}"></audio>
<audio id="audioTryAgain" src="{{ asset('/sounds/try_again.mp3') }}"></audio>
<audio id="audioIncomplete" src="{{ asset('/sounds/incomplete.wav') }}"></audio>
<audio id="audioEmpty" src="{{ asset('/sounds/empty_drawing.wav') }}"></audio>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="{{ asset('vendor/sketch/DrawingArea.js') }}"></script>
<script>
    let ornaments = JSON.parse(@json($picture->ornaments)) || [];
    const guideHistory = JSON.parse(@json($picture->guide));
</script>
<script src="{{ asset('src/admin/pictures/play.js') }}"></script>
</body>
</html>