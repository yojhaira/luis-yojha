@extends('layouts.app')

@section('content')
    <div class="example">
        <div id="lenguaje">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 panel-title-container">
                        <img class="level-thumb-img" src="{{ $category->image_url }}" alt="{{ $category->name }}">
                    </div>
                </div>
            </div>
        </div>
        <div id="lenguaje-thumb-div" class="thumbnail-container collapse show">
            <div class="container">
                <div class="row level-thumb-row">
                    <div class="col-xl-12 col-12">
                        <div class="row">
                            @foreach($category->pictures as $picture)
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                    <div class="card thumbnail-card">
                                        <a class="thumbnail-item-link" href="{{ url('category/'.$category->id.'/pictures/'.$picture->id) }}" style="text-decoration: none;">
                                            <img class="img-fluid thumb-img" src="{{ $picture->image_url }}" alt="{{ $picture->name }}">
                                            <div class="card-body thumbnail-card-body lenguaje-card">
                                                <p class="thumbnail-name">{{ $picture->name }}</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="row mt-5">
                            <div class="col-12 text-center game-icons" id="inicio">
                                <a href="/">
                                    <img class="icon-img" src="{{ asset('images/inicio.png') }}"
                                         width="70" height="70"
                                         alt="Botón para volver al inicio">
                                </a>
                                <p class="icon-text">Inicio</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection