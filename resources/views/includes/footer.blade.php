<div class="footer" id="contactanos" >
    <div class="container">
        <div class="row">
            <div class="col-12 block-footer">  
                <p id="copyright-txt">Copyright © {{ date("Y") }} Fipdeby School - Todos los derechos reservados.</p>
            </div>
        </div>
    </div>
</div>