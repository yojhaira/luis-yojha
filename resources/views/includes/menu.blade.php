
    <header class="display-none" id="main-header">
        <nav class="navbar navbar-expand-md navbar-dark py-0 nav-top justify-content-between" style="">
            <a class="navbar-brand py-3" href="/home">
                <img src="{{ asset('/images/LOGO (1).png') }}" alt="Logo" id="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="header-lists">
                <div class="navbar-collapse flex-md-column header-lists-user" id="" >
                    <ul class="navbar-nav nav-row py-0 ml-auto" id="nav-bottom">
                        {{--<li class="nav-item2 py-0" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                            {{--<a class="nav-link active py-0" href="#">Canciones</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item2 dropdown py-0">--}}
                            {{--<a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">--}}
                                {{--Cuentos<span class="sr-only">(current)</span>--}}
                            {{--</a>--}}
                            {{--<div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                                {{--<a class="dropdown-item" href="/cuentos-infantiles-cortos">Cuentos infantiles cortos </a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-clasicos-infantiles">Cuentos clásicos </a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-para-dormir">Cuentos para dormir </a>--}}
                                {{--<a class="dropdown-item" href="/fabulas-para-niños">Fábulas cortas</a>--}}
                                {{--<a class="dropdown-item" href="/mitos-y-leyendas-para-niños">Mitos y leyendas</a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-en-ingles">Cuentos en inglés</a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-inventados">Cuentos inventados</a>--}}
                                {{--<a class="dropdown-item" href="/poemas-para-niños">Poemas para niños</a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-hermanos-grimm">Cuentos hermanos Grimm</a>--}}
                                {{--<a class="dropdown-item" href="/cuentos-del-mundo">Cuentos del mundo</a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item2 dropdown py-0">--}}
                            {{--<a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">--}}
                                {{--Abecedario <span class="sr-only">(current)</span>--}}
                            {{--</a>--}}
                            {{--<div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                                {{--<a class="dropdown-item" href="/juegos-de-vocales">Juegos de vocales</a>--}}
                                {{--<a class="dropdown-item" href="/juegos-del-abecedario">Juegos del abecedario </a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item2 dropdown py-0">--}}
                            {{--<a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">--}}
                                {{--Matemática <span class="sr-only">(current)</span>--}}
                            {{--</a>--}}
                            {{--<div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                                {{--<a class="dropdown-item" href="/juegos-de-matematicas">Juegos de matemáticas</a>--}}
                                {{--<a class="dropdown-item" href="/juegos-de-numeros">Juegos de números </a>--}}
                                {{--<a class="dropdown-item" href="/juegos-de-figuras-geometricas">Juegos de figuras geométricas</a>--}}
                                {{--<a class="dropdown-item" href="/juegos-de-sumas">Juegos de sumas </a>--}}
                                {{--<a class="dropdown-item" href="/juegos-de-restas">Juegos de restas</a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item2 dropdown py-0">--}}
                            {{--<a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">--}}
                                {{--Inglés <span class="sr-only">(current)</span>--}}
                            {{--</a>--}}
                            {{--<div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                                {{--<a class="dropdown-item" href="/juegos-de-ingles">Juegos de inglés </a>--}}
                                {{--<a class="dropdown-item" href="/colores-en-ingles">Los colores en inglés </a>--}}
                                {{--<a class="dropdown-item" href="/numeros-en-ingles">Los números en inglés </a>--}}
                                {{--<a class="dropdown-item" href="/abecedario-en-ingles">El abecedario en inglés</a>--}}
                                {{--<a class="dropdown-item" href="/vocabulario-en-ingles">Vocabulario en inglés</a>--}}
                                {{--<a class="dropdown-item" href="/adivinanzas-en-ingles">Adivinanzas en inglés</a>--}}
                                {{--<a class="dropdown-item" href="/trabalenguas-en-ingles">Trabalenguas en inglés</a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item2 dropdown py-0">--}}
                            {{--<a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">--}}
                                {{--Ciencia <span class="sr-only">(current)</span>--}}
                            {{--</a>--}}
                            {{--<div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show">--}}
                                {{--<a class="dropdown-item" href="/ciencia-para-niños">Ciencia para niños </a>--}}
                                {{--<a class="dropdown-item" href="/experimentos-caseros-para-niños">Experimentos caseros </a>--}}
                                {{--<a class="dropdown-item" href="/ciencias-tecnologia">Ciencia y tecnología </a>--}}
                                {{--<a class="dropdown-item" href="/imagenes-de-animales">Animales para niños </a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        @guest
                            <li class="nav-item2 py-0" data-toggle="collapse" data-target=".navbar-collapse.show">
                                @if (Route::has('login'))
                                    <a class="nav-link active py-0" href="{{ route('login') }}">Iniciar yojhaira</a> <!--- ACA ES EL INICIAR SESION --->
                               <!--- @endif
                            </li>
                            <li class="nav-item2 py-0" data-toggle="collapse" data-target=".navbar-collapse.show">
                                @if (Route::has('login'))
                                    <a class="nav-link active py-0" href="{{ route('register') }}">Registrar</a>
                                     @endif --->
                            </li>
                        @else
                            <li class="nav-item2 dropdown py-0">
                                <a class="nav-link active dropdown-toggle py-0" data-toggle="dropdown" href="#">
                                    {{ Auth::user()->name }}<span class="sr-only">(current)</span>
                                </a>
                                <div class="dropdown-menu" data-toggle="collapse" data-target=".navbar-collapse.show" style="left: -4em;">
                                    @if(Auth::user()->is_admin)
                                        <a class="dropdown-item" href="{{ url('admin/categories') }}">Categorías</a>
                                        <a class="dropdown-item" href="{{ url('admin/pictures') }}">Dibujos</a>
                                        <a class="dropdown-item" href="{{ route('register') }}">Registrar</a> 
                                    @endif
                                    @if(Auth::user()->is_teacher)
                                        <a class="dropdown-item" href="{{ url('teacher/students') }}">Alumnos </a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/home">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/home#section2">Nosotros</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/home#section3">Actividades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/home#contactanos">Contáctanos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        {{--<nav class="navbar navbar-expand nav-bottom navbar-dark">--}}
            {{--<ul class="navbar-nav nav-row py-0 mr-auto ml-auto" id="nav-top">--}}
                {{--<li class="nav-item dropdown py-0" id="nuestra-portal">--}}
                    {{--<a class="nav-link active dropdown-toggle py-0 nav-test" data-toggle="dropdown" href="/juegos-para-ninos-de-preescolar">--}}
                        {{--Nuestro Portal <span class="sr-only">(current)</span>--}}
                    {{--</a>--}}
                    {{--<div class="dropdown-menu">--}}
                        {{--<a class="dropdown-item" href="/nuestra-historia">Nuestra Historia</a>--}}
                        {{--<a class="dropdown-item" href="/metodologia">Metodología</a>--}}
                        {{--<a class="dropdown-item" href="/curriculo">Currículo</a>--}}
                        {{--<a class="dropdown-item" href="/mision-social">Misión Social</a>--}}
                        {{--<a class="dropdown-item" href="/frases-de-educacion">Frases Educativas</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="nav-item py-0">--}}
                    {{--<a class="nav-link active py-0" href="/juegos-para-ninos-de-preescolar">Niños 3 a 5</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item py-0">--}}
                    {{--<a class="nav-link active py-0" href="/juegos-para-ninos-6-y-7-anos">Niños 5 a 7</a>--}}
                {{--</li><li class="nav-item py-0">--}}
                    {{--<a class="nav-link active py-0" href="/lecturas-cortas-para-niños">Niños 7 a 10</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</nav>--}}
</header>

@section('scripts')
<script>
    $(window).on("scroll", function() {
        if($(window).scrollTop() > 50) {
            $("#main-header").addClass("activeHeader");
            console.log("aaaaaa");
            
        } else {
            //remove the background property so it comes transparent again (defined in your css)
        $("#main-header").removeClass("activeHeader");
        }
    });
</script>
@endsection