@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Reportes para el alumno: {{ $student->name }}</h1>
        </div>

        <h2 id="landing-subtitle">Estadísticas de precisión (cantidad de grafías bien hechas vs mal hechas)</h2>
        <div class="col-sm-6">
            <div id="hits"></div>
        </div>
        <div class="col-sm-6">
            <div id="mistakes"></div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12">
            <h2 id="landing-subtitle">Ejercicios por completar</h2>
        </div>
        <div class="col-sm-12">
            <div id="levels" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto;"></div>
        </div>

        <div class="col-xl-12 col-lg-12 col-md-12">
            <h2 id="landing-subtitle">Número de intentos realizados por cada grafía</h2>
        </div>
        <div class="col-sm-12">
            <div id="attempts" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto;"></div>
        </div>

        <div class="col-xs-12 col-12 mt-5">
            <div class="container-fluid text-center">
                <a href="{{ url('/teacher/students') }}">
                    <img class="icon-img" src="https://arbolabc.nyc3.cdn.digitaloceanspaces.com/General/Buttons/back_btncolor.png" alt="Botón para regresar a la lista de alumnos">
                </a>
                <p class="icon-text">Regresar</p>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

    <script src="{{ asset('src/admin/reports/hits-mistakes.js') }}"></script>
    <script>
        $(function () {
            renderHitsAndMistakes({{ $gaugeReport['hits'] }}, {{ $gaugeReport['mistakes'] }});
        });
    </script>

    <script src="{{ asset('src/admin/reports/levels.js') }}"></script>
    <script>
        $(function () {
            renderLevels(@json($levelsReport));
        });
    </script>

    <script src="{{ asset('src/admin/reports/attempts.js') }}"></script>
    <script>
        $(function () {
            renderAttempts(@json($attemptsReport));
        });
    </script>
@endsection