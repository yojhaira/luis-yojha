@extends('layouts.app')

@section('content')
        <div class="container row" id="title-elems-row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <h1 id="landing-title">Lista de alumnos</h1>
                <a href="{{ url('teacher/students/create') }}" class="btn btn-new btn-primary">Nuevo alumno</a>
                <table class="table">
                    <thead>
                    <tr class="table-active">
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($students as $student)
                        <tr>
                            <td>{{ $student->name }}</td>
                            <td>{{ $student->email }}</td>
                            <td>
                                <a href="{{ url('/teacher/students/'.$student->id.'/reports') }}" class="btn btn-sm btn-info" title="Reportes"><i class="fa fa-pie-chart"></i></a>
                                <a href="{{ url('/teacher/students/'.$student->id.'/edit') }}" class="btn btn-sm btn-success" title="Editar alumno"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('/teacher/students/'.$student->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar almuno"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $students->links() }}
            </div>
        </div>
@endsection
