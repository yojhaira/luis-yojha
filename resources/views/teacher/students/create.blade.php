@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Nuevo alumno</h1>
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif
            <div id="alert"></div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input name="name" type="text" class="form-control" id="name" placeholder="Ingrese nombre" value="{{ old('name') }}" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="email" type="email" class="form-control" id="email" placeholder="Ingrese email" value="{{ old('email') }}" required>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input name="password" type="password" class="form-control" id="password" required>
                    <small id="fileHelp" class="form-text text-muted">Al menos 6 caracteres</small>
                </div>
                
                <a href="{{ url('teacher/students') }}" class="btn btn-new btn-dark">Cancelar</a>
                <button type="submit" class="btn btn-new btn-primary">Registrar</button>
            </form>
        </div>
    </div>
@endsection
