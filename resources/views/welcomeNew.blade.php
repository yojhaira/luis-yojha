@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <section class="bg-blue block-welcome roww">
            <div class="col-md-12">
                <div class="row block-top">
                    <div class="col figure-welcome size-2">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/letra (3).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/colores (5).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1" >
                        <img class="animated bounceInDown delay-3s" src="/images/bienvenida/colores (1).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img  class="animated bounceInDown delay-5s"src="/images/bienvenida/crayola (1).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img  class="animated bounceInDown delay-2s"src="/images/bienvenida/crayola (2).png" alt="">
                    </div>
                    <div class="col figure-welcome size-2">
                        <img  class="animated bounceInDown delay-4s"src="/images/bienvenida/regla.png" alt="" >
                    </div>

                    <div class="col figure-welcome size-1">
                        <img  class="animated bounceInDown delay-3s"src="/images/bienvenida/num (1).png" alt="" >
                    </div>

                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/colores (4).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-3s" src="/images/bienvenida/colores (2).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-5s" src="/images/bienvenida/clic (2).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-2s" src="/images/bienvenida/tijera.png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-4s" src="/images/bienvenida/colores (3).png" alt="">
                    </div>

                    <div class="col figure-welcome size-1">
                        <img  class="animated bounceInDown delay-3s"src="/images/bienvenida/num (3).png" alt="" >
                    </div>

                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/crayola (1).png" alt="">
                    </div>
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-3s" src="/images/bienvenida/lupa.png" alt="">
                    </div>
                    
                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-1.5s" src="/images/bienvenida/letra (1).png" alt="">
                    </div>

                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/clic (2).png" alt="">
                    </div>
                    <div class="col figure-welcome size-3">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/e.png" alt="">
                    </div>
                    <div class="col figure-welcome size-3">
                        <img class="animated bounceInDown delay-3s" src="/images/bienvenida/f.png" alt="">
                    </div>
                    <div class="col figure-welcome size-3">
                        <img class="animated bounceInDown delay-1s" src="/images/bienvenida/p.png" alt="">
                    </div>

                    <div class="col figure-welcome size-1">
                        <img class="animated bounceInDown delay-2s" src="/images/bienvenida/letra (2).png" alt="">
                    </div>
                </div>
            </div>

            <div class="col-md-12 block-img-welcome">
                <div class="container">
                    <div class="row align-items-center width-100">
                        <div class="col-md-8 text-white">
                            <h1 class="animated fadeInLeft title-welcome">Bienvenidos</h1><br>
                            <img src="/images/LOGO (1).png" alt="" style="width: 400px;" class="img-fluid animated fadeInLeft delay-1s">
                        </div>
                        <div class="col-md-4 text-center d-none d-md-block">
                            <img src="/images/bienvenida/mochi.png" alt="" class="animated bounceInDown delay-1s img-banner">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $("#main-header").hide();
        $(".footer").hide();
        setTimeout(() => {
            window.location.pathname='/home';
        }, 4000);
    </script>
@endsection