@extends('layouts.app')

@section('content')
    <section class="bg-login block-login">
        <div class="container-fluid height-100">
            <div class="row height-100">
                <div class="col-lg-6"></div>
                <div class="col-lg-6 text-white">
                    <div class="row height-100" id="title-elems-row">
                        <div class="col-lg-8 offset-lg-2 d-flex align-items-c">
                            <div class="width-100">
                                <h3>Iniciar YUUN</h3>
                                @if (session('notification'))
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×UUUUU</button>
                                        {{ session('notification') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input name="email" type="email" class="form-control" id="email" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Contraseña</label>
                                        <input name="password" type="password" class="form-control" id="password" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                Recordar sesión
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary">
                                            Ingresar
                                        </button>

                                        {{--@if (Route::has('password.request'))--}}
                                            {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                                                {{--¿Olvidaste tu contraseña?--}}
                                            {{--</a>--}}
                                        {{--@endif--}}
                                    </div>
                                    <!--- <div class="row" style="margin-top: 20px">
                                        <div class="form-group">
                                            <p class="text-muted">¿No tienes una cuenta? <a href="{{ route('register') }}" class="text-primary m-l-5"><b>Regístrate</b></a></p>
                                        </div>
                                    </div>  -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
