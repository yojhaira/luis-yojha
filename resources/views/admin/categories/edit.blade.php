@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Editar categoría</h1>
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif
            <div id="alert"></div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese nombre" value="{{ $category->name }}" required>
                </div>
                <div class="form-group">
                    <label for="description">Descripción <small>(opcional)</small></label>
                    <textarea class="form-control" name="description" id="description" rows="3">{{ $category->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="image">Imagen representativa</label>
                    <br>
                    <img src="{{ $category->image_url }}" alt="Imagen representativa" style="height: 100px">
                    <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Ingresar imagen si solo desea cambiar (tamaño de 150x120).</small>
                </div>
                <a href="{{ url('admin/categories') }}" class="btn btn-new btn-dark">Cancelar</a>
                <button type="submit" class="btn btn-new btn-success">Guardar cambios</button>
            </form>
        </div>
    </div>
@endsection
