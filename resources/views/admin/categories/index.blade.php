@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Lista de categorías</h1>
            <a href="{{ url('admin/categories/create') }}" class="btn btn-new btn-primary">Nueva categoría</a>
            <table class="table">
                <thead>
                <tr class="table-active">
                    <th scope="col">Imagen</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            <img src="{{ $category->image_url }}" alt="Imagen representativa" style="max-height: 40px">
                        </td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->descripción }}</td>
                        <td>
                            <a href="{{ url('/admin/categories/'.$category->id.'/edit') }}" class="btn btn-sm btn-success" title="Editar categoría"><i class="fa fa-edit"></i></a>
                            <a href="{{ url('/admin/categories/'.$category->id.'/delete') }}" class="btn btn-sm btn-danger" title="Eliminar categoría" onclick="return confirm('¿Seguro que desea eliminar esta categoría?')"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>

        <div class="col-12 mt-5 text-center game-icons" id="inicio">
            <a href="/">
                <img class="icon-img" src="{{ asset('images/inicio.png') }}" alt="Botón para volver al inicio">
            </a>
            <p class="icon-text">Inicio</p>
        </div>
    </div>
@endsection
