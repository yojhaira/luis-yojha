@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Lista de dibujos</h1>
            <a href="{{ url('admin/pictures/create') }}" class="btn btn-new btn-primary">Nuevo dibujo</a>
            <form action="" method="GET">
                <div class="form-group">
                    <label for="category_id">Búsqueda por categoría</label>
                    <select class="form-control" id="category_id" name="category_id">
                        <option value="">Seleccione categoria</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}" {{ $categoryId == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" id="btnSearch" style="display: none"></button>
            </form>
            <table class="table">
                <thead>
                <tr class="table-active">
                    <th scope="col">Imagen</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pictures as $picture)
                    <tr>
                        <td>
                            <img src="{{ $picture->image_url }}" alt="Imagen representativa" style="max-height: 40px">
                        </td>
                        <td>{{ $picture->name }}</td>
                        <td>{{ $picture->descripción }}</td>
                        <td>
                            <a href="{{ url('/admin/pictures/'.$picture->id.'/edit') }}" class="btn btn-sm btn-success" title="Editar datos del dibujo">
                                <i class="fa fa-file"></i>
                            </a>
                            <a href="{{ url('/admin/pictures/'.$picture->id.'/guide') }}" class="btn btn-sm btn-info" title="Definir trazo guía">
                                <i class="fa fa-pencil-alt"></i>
                            </a>
                            <a href="{{ url('/admin/pictures/'.$picture->id.'/images') }}" class="btn btn-sm btn-warning" title="Añadir imágenes de adorno">
                                <i class="fa fa-images"></i>
                            </a>
                            <a href="{{ url('/admin/pictures/'.$picture->id.'/delete') }}" class="btn btn-sm btn-danger" onclick="return confirm('¿Seguro que desea eliminar este dibujo?')" title="Eliminar dibujo">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $pictures->links() }}
        </div>

        <div class="col-12 mt-5 text-center game-icons" id="inicio">
            <a href="/">
                <img class="icon-img" src="{{ asset('images/inicio.png') }}" alt="Botón para volver al inicio">
            </a>
            <p class="icon-text">Inicio</p>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(() => {
            $('#category_id').on('change', onClickButton);
        });
        function onClickButton() {
            $('#btnSearch').click();
        }
    </script>
@endsection