<!doctype html>
<html>
<head>
    <title>Editar guía | Dibujo {{ $picture->id }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="{{ asset('vendor/sketch/sketch.css') }}">
</head>
<body>

<div id="page" data-action="{{ url('admin/pictures/'.$picture->id.'/guide') }}" data-csrf="{{ csrf_token() }}">
    <div class="header">
        <div class="title">Editar guía del dibujo "{{ $picture->name }}"</div>
    </div>

    <div id="content">
        <p style="text-align:center">Cargando Canvas ...</p>
    </div>

    <div class="footer">
        <div class="buttons buttons-4">
            <a class="navbtn" onclick="newCanvas()">Limpiar</a>
            <a class="navbtn" onclick="loadImage()">Importar imagen</a>
            <input type="file" id="inputImage" style="display: none">
            <a class="navbtn" onclick="saveCanvas()">Guardar y volver</a>
            <a class="navbtn" href="{{ url('admin/pictures/') }}">Volver sin guardar</a>
        </div>
    </div>
</div>

<script src="{{ asset('vendor/sketch/DrawingArea.js') }}"></script>
<script>
    let myHistory = JSON.parse(@json($picture->guide));
</script>
<script src="{{ asset('src/admin/pictures/guide.js') }}"></script>
</body>
</html>