@extends('layouts.app')

@section('content')
    <div class="container row" id="title-elems-row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <h1 id="landing-title">Nuevo dibujo</h1>
            @if (session('notification'))
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('notification') }}
                </div>
            @endif
            <div id="alert"></div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese nombre" value="{{ old('name') }}" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="category_id">Seleccione categoría</label>
                        <select class="form-control" id="category_id" name="category_id" required>
                            <option value="">Seleccione categoria</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Descripción <small>(opcional)</small></label>
                    <textarea class="form-control" name="description" id="description" rows="3">{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="image">Imagen</label>
                    <input type="file" class="form-control-file" id="image" name="image" aria-describedby="fileHelp" required>
                    <small id="fileHelp" class="form-text text-muted">Seleccione imagen formato .svg</small>
                </div>
                <div class="form-group">
                    <label for="image">Video guía</label>
                    <input type="file" class="form-control-file" id="video" name="video" aria-describedby="file" accept="video/mp4" required>
                    <small id="file" class="form-text text-muted">Seleccione imagen formato .mp4</small>
                </div>
                <a href="{{ url('admin/pictures') }}" class="btn btn-new btn-dark">Cancelar</a>
                <button type="submit" class="btn btn-new btn-primary">Registrar</button>
            </form>
        </div>
    </div>
@endsection
