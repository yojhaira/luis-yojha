@extends('layouts.app')

@section('content')
    <div class="example">
        <div class="row" id="game-content">
            <div class="col-xl-12 col-sm-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a class="breadcrumb-link" href="/">Inicio</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a class="breadcrumb-link" href="{{ url('category/'.$category->id.'/pictures') }}">{{ $category->name }}</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $picture->name }}</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-12 text-center">
                            <iframe width="560" height="315" src="{{ $picture->video_url }}" frameborder="0" allowfullscreen>
                            </iframe>

                            @if ($picture->description)
                                <p>{{ $picture->description }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12" id="icon-bar">
                            <div class="row">
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-3"> </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6" id="btn-row">
                                    <div class="row">
                                        <div class="col-6 game-icons" id="inicio">
                                            <a href="/">
                                                <img class="icon-img" src="{{ asset('images/inicio.png') }}" alt="Botón para volver al inicio">
                                            </a>
                                            <p class="icon-text">Inicio</p>
                                        </div>
                                        <div class=" col-6 game-icons" id="inicio">
                                            <a href="{{ url('/pictures/'.$picture->id.'/play') }}">
                                                <img class="icon-img" src="{{ asset('images/play.png') }}" alt="Botón para iniciar el juego" title="Clic para iniciar">
                                            </a>
                                            <p class="icon-text">Empezar</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection