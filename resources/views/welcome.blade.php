@extends('layouts.app')

@section('content')


    <section class="bg-blue block-init">
        <div class="container-fluid" style="height: inherit;">
            <div class="row" style="height: 90%;">
                <div class="col-lg-3 col-md-3 col-sm-3 text-white block-top-init-text">
                    <h1 class="animated fadeInLeft title-welcome" align="center"> Vamos al cole...</h1><br>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 block-top-init">
                    <img class="animated bounceInDown title-welcome" src="/images/inicio/ninos_inicio.png" alt="" id="img-init-main">
                </div>
            </div>
        </div>
    </section>

    <section class="container mt-3">
        <div class="row align-items-center" id="section2">
            <div class="col-lg-6">
                <h3 class="subtitle-home">¿En qué consiste?</h3>
                <p>Fipdebys School, es un sistema de caracterizacion y Evaluacion automatica de grafismos, el cual de forma dinamica y divertida ayudará a los niños de pregrado a desarrollar su pscimotricidad fina, de la misma forma que va reconociendo, memorizando y explorando lo que practica.
                    El proceso del sistema consiste en trazar el grafismo que parece en la pantalla de la pagina Web; este trazo se realiza con la ayuda de un lapiz digital en la tableta de dibujo. Posteriormente el sistema llevara acabo la evaluacion del trazo, el cual consiste en la comparacion de la imagen del grafismo mostrado en pantalla con el trazo realizado por el niño(a). </p>
            </div>
            <div class="col-lg-6 text-center">
                <img src="/images/inicio/nosotros.png" alt="" style="max-width:500px" class="img-fluid-100">
            </div>
            
            <!--- <i class="fas fa-chevron-up"></i> -->
        </div>
        
        <div class="row" id="section3">
            <div class="col-lg-12">
                <h3 class="subtitle-home">Nuestras actividades</h3>
                <p class="mb-4">Fipdebys School, te brinda un metodo dinamico y divertido de practicar la sicomotricidad fina de los niños de pregrado en los temas basicos que se les enseña en aula de acuerdo a su edad, como son las vocales, el abecedario, las figuras geometricas y numeros de una cifra, asimismo el niño(a).</p>
                <p>Para disfrutar de estos beneficios debes <a href="/login">iniciar sesion</a></p>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="row justify-content-md-center">
                    <div class="col-lg-10 col-md-12">
                        <div class="row text-center">
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                <a href="/login">
                                    <img src="/images/inicio/opciones/geometricas.png" alt="" class="img-fluid" style="max-width:100%" >
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                <a href="/login">
                                    <img src="/images/inicio/opciones/comunicacion.png" alt="" class="img-fluid" style="max-width:100%">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                <a href="/login">
                                    <img src="/images/inicio/opciones/numeros.png" alt="" class="img-fluid" style="max-width:100%" >
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 mb-3">
                                <a href="/editor">
                                    <img src="/images/inicio/opciones/practica.png" alt="" class="img-fluid" style="max-width:100%">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($categories as $category)
                                <div class="col-4">
                                    <a class="icon-link" href="{{ url('category/'.$category->id.'/pictures') }}">
                                        <img class="level-header-icons" src="{{ $category->image_url }}"
                                            title="Ingresa a la categoría {{ $category->name }}"
                                            alt="Ícono para la categoría de {{ $category->name }}">
                                    </a>
                                </div>
                            @endforeach
                            <div class="col-4 text-center">
                                <a class="icon-link" href="/editor">
                                    <img class="level-header-icons" src="/images/editor-libre.png" width="120"
                                        alt="Ícono para el editor libre"
                                        title="Editor libre">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(window).on("scroll", function() {
            if($(window).scrollTop() > 50) {
                $("#main-header").addClass("activeHeader");
                console.log("aaaaaa");
                
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $("#main-header").removeClass("activeHeader");
            }
        });

        const audio = document.getElementById('myAudioHover');
        let lastHoveredUrl = null;

        $(() => {
            listenCategoriesHoverEvent();
        });

        function listenCategoriesHoverEvent() {
            $('#categories a.icon-link').hover(e => {
                const currentUrl = $(e.target).parent().attr('href');
                if (currentUrl !== lastHoveredUrl) {
                    audio.pause();
                    audio.currentTime = 0;

                    audio.play().catch(() => {
                        console.log('Still loading');
                    });
                    lastHoveredUrl = currentUrl;
                }
            });
        }
    </script>
@endsection