<?php

Auth::routes();

Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/editor', 'WelcomeController@editor');
Route::get('/home', 'WelcomeController@home');


Route::get('/init', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth','admin'], 'namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::get('categories', 'CategoryController@index');
    Route::get('categories/create', 'CategoryController@create');
   
    Route::post('categories/create', 'CategoryController@store');
    Route::get('categories/{id}/edit', 'CategoryController@edit');
    
    Route::post('categories/{id}/edit', 'CategoryController@update');
    Route::get('categories/{id}/delete', 'CategoryController@delete');

    Route::get('pictures', 'PictureController@index');
    Route::get('pictures/create', 'PictureController@create');
    Route::post('pictures/create', 'PictureController@store');
    Route::get('pictures/{id}/edit', 'PictureController@edit');
    Route::post('pictures/{id}/edit', 'PictureController@update');
    Route::get('pictures/{picture}/guide', 'PictureController@getGuide');
    Route::post('pictures/{picture}/guide', 'PictureController@postGuide');
    Route::get('pictures/{picture}/images', 'PictureImageController@index');
    Route::post('pictures/ornament', 'PictureImageController@upload');
    Route::post('pictures/{picture}/images', 'PictureImageController@store');
    Route::get('pictures/{id}/delete', 'PictureController@delete');

    //Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    //Route::post('register', 'Auth\RegisterController@register');
});

Route::group(['middleware' => ['auth','teacher'], 'namespace' => 'Teacher', 'prefix' => 'teacher'], function () {
    Route::get('students', 'StudentController@index');
    Route::get('students/create', 'StudentController@create');
    Route::post('students/create', 'StudentController@store');
    Route::get('students/{id}/edit', 'StudentController@edit');
    Route::post('students/{id}/edit', 'StudentController@update');
    Route::get('students/{id}/delete', 'StudentController@delete');
    Route::get('students/{id}/reports', 'StudentController@reports');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('category/{id}/pictures', 'CategoryController@index');
    Route::get('category/{category_id}/pictures/{picture_id}', 'ExerciseController@index');

    Route::get('pictures/{picture}/play', 'ExerciseController@play');
    Route::post('pictures/{pictureId}/play', 'ExerciseController@store');
    Route::get('/next', 'ExerciseController@next');
});
